package pm.gui;

import java.io.IOException;
import java.util.ArrayList;
import saf.ui.AppGUI;
import saf.AppTemplate;
import saf.components.AppWorkspaceComponent;
import saf.settings.AppPropertyType;
import javafx.scene.layout.Pane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Paint;
import javafx.geometry.Point2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import static pm.PropertyType.REMOVE_ICON;
/**
 * This class serves as the workspace component for this application, providing
 * the user interface controls for editing work.
 *
 * @author Richard McKenna
 * @author ?
 * @version 1.0
 */
public class Workspace extends AppWorkspaceComponent {

    // HERE'S THE APP
    AppTemplate app;

    // IT KNOWS THE GUI IT IS PLACED INSIDE
    AppGUI gui;
    HBox createShapes;
    
    BorderPane leftPane;
    VBox sideBar;
    
    Image removeIcon;
    ArrayList<Point2D> shapesLocation;
    
    
    /**
     * Constructor for initializing the workspace, note that this constructor
     * will fully setup the workspace user interface for use.
     *
     * @param initApp The application this workspace is part of.
     *
     * @throws IOException Thrown should there be an error loading application
     * data for setting up the user interface.
     */
    public Workspace(AppTemplate initApp) throws IOException {
	// KEEP THIS FOR LATER
	app = initApp;

	// KEEP THE GUI FOR LATER
	gui = app.getGUI();
        
        
        createShapes = new HBox();
        workspace = new BorderPane();
        
        leftPane = new BorderPane();
        sideBar = new VBox();
        
        
        
        removeIcon = new Image(REMOVE_ICON);
        
        ArrayList<Button> utilButtons = new ArrayList();    
        
        Button removeShape = new Button();
        removeShape.
        utilButtons.add(removeShape);
        Button moveShape = new Button();
        utilButtons.add(moveShape);
        Button makeSquare = new Button();
        utilButtons.add(makeSquare);
        Button makeElipse = new Button();
        utilButtons.add(makeElipse);
        
        for(Button utilButton : utilButtons){
            createShapes.getChildren().add(utilButton);
        }
        leftPane.setTop(createShapes);

        
        workspace.getChildren().add(leftPane);
        
        
        
        
    }
    
    /**
     * This function specifies the CSS style classes for all the UI components
     * known at the time the workspace is initially constructed. Note that the
     * tag editor controls are added and removed dynamicaly as the application
     * runs so they will have their style setup separately.
     */
    @Override
    public void initStyle() {
	// NOTE THAT EACH CLASS SHOULD CORRESPOND TO
	// A STYLE CLASS SPECIFIED IN THIS APPLICATION'S
	// CSS FILE
        
        //createShapes.getStyleClass().add("color_chooser_pane");
    }

    /**
     * This function reloads all the controls for editing tag attributes into
     * the workspace.
     */
    @Override
    public void reloadWorkspace() {

    }
}
