package pm.gui;


import java.awt.Image;
import java.awt.Point;
import java.awt.image.RenderedImage;
import java.io.File;
import javafx.scene.shape.Shape;
import java.io.IOException;
import java.util.ArrayList;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.EventType;
import javafx.geometry.Orientation;
import javafx.geometry.Point2D;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.layout.FlowPane;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Slider;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javax.imageio.ImageIO;
import pm.PoseMaker;

import pm.controller.PageEditController;
import pm.PropertyType;
import saf.ui.AppGUI;
import saf.AppTemplate;
import saf.components.AppWorkspaceComponent;
import properties_manager.PropertiesManager;


/**
 * This class serves as the workspace component for this application, providing
 * the user interface controls for editing work.
 *
 * @author Richard McKenna
 * @author ?
 * @version 1.0
 */
public class Workspace extends AppWorkspaceComponent {

    // HERE'S THE APP
    AppTemplate app;

    // IT KNOWS THE GUI IT IS PLACED INSIDE
    AppGUI gui;
    
    PageEditController pageEditController;

    HBox createShapes;
    HBox bringFront;
   
    VBox bgColor;
    VBox fillColor;
    VBox outlineColor;
    VBox outlineThickness;
    VBox snapshot;
    
    VBox sideBar;
    BorderPane leftPane;
    Image removeIcon;
    
    Button bringDown;
    Button bringUp;
    Button removeShape;
    
    Pane toolBar;
    Canvas canvas;
    ScrollPane toolBarScrollPane;
    
    Label bgColorLabel;
    Label fillColorLabel;
    Label outlineColorLabel;
    Label outlineThicknessLabel;
    
    ColorPicker bgColorPicker;
    ColorPicker fillColorPicker;
    ColorPicker outlineColorPicker;
    
    Slider outlineThicknessSlider;
    GraphicsContext gc;
    Group root;
    
    Color colorFill;
    Color colorOutline;
    Color colorBg;
    double thickness;
    
    String shape;

    boolean createShape;
    
    double dragX;
    double dragY;
    double newX;
    double newY;
    
    ArrayList<Rectangle> rectangles;
    ArrayList<Ellipse> ellipses;
    /**
     * Constructor for initializing the workspace, note that this constructor
     * will fully setup the workspace user interface for use.
     *
     * @param initApp The application this workspace is part of.
     *
     * @throws IOException Thrown should there be an error loading application
     * data for setting up the user interface.
     */
    public Workspace(AppTemplate initApp) throws IOException {
	// KEEP THIS FOR LATER
	app = initApp;

	// KEEP THE GUI FOR LATER
	gui = app.getGUI();
        PropertiesManager propsSingleton = PropertiesManager.getPropertiesManager();
       
        pageEditController = new PageEditController((PoseMaker) app);
        
        workspace = new BorderPane();
        
        rectangles = new ArrayList();
        ellipses = new ArrayList();
        
        createShapes = new HBox();
        leftPane = new BorderPane();
        canvas = new Canvas();
        gc = canvas.getGraphicsContext2D();
        
        root = new Group();
        root.getChildren().add(canvas);
        
        toolBar = new FlowPane(Orientation.VERTICAL);
        toolBar.setMinHeight(1920);          //SET USER RESOLUTION?!?!?
        //toolBarScrollPane = new ScrollPane(toolBar);  NOT NEEDED?
        //toolBarScrollPane.setFitToHeight(true);
        
        
        toolBar.getChildren().add(createShapes);
        ArrayList<Button> utilButtons = new ArrayList();
        
        removeShape = gui.initChildButton(createShapes, PropertyType.REMOVE_ICON.toString(), PropertyType.REMOVE_TOOLTIP.toString(), false);
        removeShape.setOnAction(e ->{
            createShape = false;
                Shape check = pageEditController.isHighlighted();
                check.setStyle("");
                pageEditController.removeShape(check);
                if(check.getClass().toString().contains("rectangle")){
                    rectangles.remove(check);
                }
                if(check.getClass().toString().contains("ellipse")){
                    ellipses.remove(check);
                }
                refreshButtons();
        });
        Button selectShape = gui.initChildButton(createShapes, PropertyType.SELECTION_TOOL_ICON.toString(), PropertyType.SELECTION_TOOL_TOOLTIP.toString(), false);
        selectShape.setOnAction(e ->{
            createShape = false;
            workspace.setCursor(Cursor.DEFAULT);
        });
        Button makeRectangle = gui.initChildButton(createShapes, PropertyType.RECTANGLE_ICON.toString(), PropertyType.RECTANGLE_TOOLTIP.toString(), false);
        makeRectangle.setOnAction(e ->{
            createShape = true;
            shape = "rectangle";
            workspace.setCursor(Cursor.CROSSHAIR);
            try{
                Shape check = pageEditController.isHighlighted();
                check.setStyle("");
            }
            catch(Exception ex){
                
            }
        });
        Button makeEllipse = gui.initChildButton(createShapes, PropertyType.ELLIPSE_ICON.toString(), PropertyType.ELLIPSE_TOOLTIP.toString(), false);
        makeEllipse.setOnAction(e ->{
            createShape = true;
            shape = "ellipse";
            workspace.setCursor(Cursor.CROSSHAIR);
            try{
                Shape check = pageEditController.isHighlighted();
                check.setStyle("");
            }
            catch(Exception ex){
                
            }
        });
        bringFront = new HBox();
        bringDown = gui.initChildButton(bringFront, PropertyType.MOVE_DOWN_ICON.toString(), PropertyType.MOVE_DOWN_TOOLTIP.toString(), false);
        bringDown.setOnAction(e -> {
            createShape = false;
            workspace.setCursor(Cursor.DEFAULT);
            Shape change = pageEditController.isHighlighted();
            change.toBack();
        });
        bringUp = gui.initChildButton(bringFront, PropertyType.MOVE_UP_ICON.toString(), PropertyType.MOVE_UP_TOOLTIP.toString(), false);
        bringUp.setOnAction(e -> {
            createShape = false;
            workspace.setCursor(Cursor.DEFAULT);
            Shape change = pageEditController.isHighlighted();
            change.toFront();
        });

        toolBar.getChildren().add(bringFront);
       
        bgColor = new VBox();
        bgColorLabel = new Label("Background color");
        bgColorLabel.setFont(new Font("Arial", 30));
        
        bgColorPicker = new ColorPicker();
        bgColor.getChildren().add(bgColorLabel);
        bgColor.getChildren().add(bgColorPicker);
        bgColorPicker.setOnAction(e->{
            String bgColorSelect = bgColorPicker.getValue().toString().substring(2, 8);
            workspace.setStyle("-fx-background-color: #" + bgColorSelect);
        });
        toolBar.getChildren().add(bgColor);
        
        fillColor = new VBox();
        fillColorLabel = new Label("Fill Color");
        fillColorLabel.setFont(new Font("Arial", 30));
        fillColorPicker = new ColorPicker();
        fillColorPicker.setOnAction(e ->{
            try{
                Shape change = pageEditController.isHighlighted();
                pageEditController.changeFillColor(change, fillColorPicker.getValue());
            }
            catch(Exception ex){}
        });
        fillColor.getChildren().add(fillColorLabel);
        fillColor.getChildren().add(fillColorPicker);
        toolBar.getChildren().add(fillColor);
        
        outlineColor = new VBox();
        outlineColorLabel = new Label("Outline Color");
        outlineColorLabel.setFont(new Font("Arial", 30));
        outlineColorPicker = new ColorPicker();
        outlineColorPicker.setOnAction(e -> {
            try{
                Shape change = pageEditController.isHighlighted();
                pageEditController.changeOutlineColor(change, outlineColorPicker.getValue());
            }
            catch(Exception ex){}
        });
        outlineColor.getChildren().add(outlineColorLabel);
        outlineColor.getChildren().add(outlineColorPicker);
        toolBar.getChildren().add(outlineColor);
        
        outlineThickness = new VBox();
        outlineThicknessLabel = new Label("Outline Thickness");
        outlineThicknessLabel.setFont(new Font("Arial", 30));
        outlineThicknessSlider = new Slider();
        outlineThicknessSlider.setOnMouseDragged(e->{
            try{
                Shape change = pageEditController.isHighlighted();
                pageEditController.changeOutlineThickness(change, outlineThicknessSlider.getValue()/10);
            }
            catch(Exception ex){}
        });
        outlineThickness.getChildren().add(outlineThicknessLabel);
        outlineThickness.getChildren().add(outlineThicknessSlider);
        toolBar.getChildren().add(outlineThickness);
        
        snapshot = new VBox();
        Button snapshotButton = gui.initChildButton(snapshot,PropertyType.SNAPSHOT_ICON.toString() , PropertyType.SNAPSHOP_TOOLTIP.toString(), false);
        snapshotButton.setOnAction(e ->{
            createShape = false;
            workspace.setCursor(Cursor.DEFAULT);
            WritableImage writableImage = new WritableImage((int)workspace.getWidth(), (int)workspace.getHeight());
            workspace.snapshot(null, writableImage);
            File snapImage = new File("./snapshots/Pose.png");
            try{

                ImageIO.write(SwingFXUtils.fromFXImage(writableImage, null), "png", snapImage);
            }
            catch(Exception ex){
            }
            
        });
        toolBar.getChildren().add(snapshot);
        
        leftPane.setLeft(toolBar);
        
        workspace = new Pane();
        workspace.setOnMousePressed(e ->{
            colorFill = fillColorPicker.getValue();
            colorOutline = outlineColorPicker.getValue();
            thickness = outlineThicknessSlider.getValue()/10;  //prevent outline from getting too thick
            
            dragX = e.getX();
            dragY = e.getY();  
            try{
                if(workspace.getCursor().equals(Cursor.DEFAULT)){
                    try{
                        Shape deSelect = pageEditController.isHighlighted();
                        deSelect.setStyle("");
                    }
                    catch(Exception ex){

                    }
                }
            }
            catch(Exception ex){
            }
            
        });
        workspace.setOnMouseReleased(e -> {
            newX = dragX-e.getX();
            newY = dragY-e.getY();
            if(createShape){
                if(shape.equals("rectangle")){
                    rectangles.add(pageEditController.handleAddSquare(new Point2D(dragX, dragY), workspace, colorFill, colorOutline, thickness, -newX, -newY));
                    reloadWorkspace();
                }
                else if(shape.equals("ellipse")){
                    ellipses.add(pageEditController.handleAddEllipse(new Point2D(e.getX(), e.getY()), workspace, colorFill, colorOutline, thickness, -newX, -newY));
                }
            }
        });

       
        bringUp.setDisable(true);
        bringDown.setDisable(true);
        removeShape.setDisable(true);
        workspace.getChildren().add(leftPane);
        workspace.getChildren().add(canvas);
        
        
        //workspaceActivated = false;
    }
    public void setOutlineColorPicker(Color val){
        outlineColorPicker.setValue(val);
    }
    public void setFillColorPicker(Color val){
        fillColorPicker.setValue(val);
    }
    public void setOutlineThicknessSlider(Double val){
        outlineThicknessSlider.setValue(val);
    }
    public Color getOutlineColorPicker(){
        return outlineColorPicker.getValue();
    }
    public Color getFillColorPicker(){
        return fillColorPicker.getValue();
    }
    public double getOutlineThicknessSlider(){
        return outlineThicknessSlider.getValue()/10;
    }
    public void setBgColor(String bgColor){
        workspace.setStyle(bgColor);
    }
    public Pane getToolbar(){
        return toolBar;
    }
    public ArrayList<Rectangle> getRectangles(){
        return rectangles;
    }
    public ArrayList<Ellipse> getEllipses(){
        return ellipses;
    }
    public void addRectangles(Rectangle r){
        rectangles.add(r);
        pageEditController.addList(r);
    }
    public void addEllipses(Ellipse e){
        ellipses.add(e);
        pageEditController.addList(e);
    }
    public void clearList(){
        ellipses.clear();
        rectangles.clear();
    }
    /**
     * This function specifies the CSS style classes for all the UI components
     * known at the time the workspace is initially constructed. Note that the
     * tag editor controls are added and removed dynamicaly as the application
     * runs so they will have their style setup separately.
     */
    @Override
    public void initStyle() {
	// NOTE THAT EACH CLASS SHOULD CORRESPOND TO
	// A STYLE CLASS SPECIFIED IN THIS APPLICATION'S
	// CSS FILE
        createShapes.getStyleClass().add(CLASS_BORDERED_PANE);
        bringFront.getStyleClass().add(CLASS_BORDERED_PANE);
        bgColor.getStyleClass().add(CLASS_BORDERED_PANE);
        fillColor.getStyleClass().add(CLASS_BORDERED_PANE);
        outlineColor.getStyleClass().add(CLASS_BORDERED_PANE);
        outlineThickness.getStyleClass().add(CLASS_BORDERED_PANE);
        snapshot.getStyleClass().add(CLASS_BORDERED_PANE);
        leftPane.getStyleClass().add("max_pane");
    }

    /**
     * This function reloads all the controls for editing tag attributes into
     * the workspace.
     */
    @Override
    public void reloadWorkspace() {
    }   
    public void refreshButtons(){
        if(pageEditController.isHighlighted() == null){
            bringUp.setDisable(true);
            bringDown.setDisable(true);
            removeShape.setDisable(true);
        }
        else{
            bringUp.setDisable(false);
            bringDown.setDisable(false);
            removeShape.setDisable(false);
        }
    }
}
