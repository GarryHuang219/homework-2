/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pm.controller;


import java.util.ArrayList;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.layout.Pane;
import pm.PoseMaker;
import pm.gui.Workspace;
import javafx.geometry.Point2D;
import javafx.scene.Group;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Label;
import javafx.scene.paint.Paint;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Ellipse;
import javafx.scene.Cursor;
import javafx.scene.shape.Shape;

/**
 *
 * @author Garry Huang
 */
public class PageEditController {
    PoseMaker app;

    private boolean enabled;
    ArrayList <Shape> sList = new ArrayList();

    public PageEditController (PoseMaker initApp){
        app = initApp;
    }
    public void enable(boolean enableSetting) {
	enabled = enableSetting;
    }
    public Rectangle handleAddSquare(Point2D location, Pane workarea, Color fillColor, Color outlineColor, double thickness, double x, double y){
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        Rectangle r = new Rectangle(location.getX(), location.getY(), x, y);
        r.setFill(fillColor);
        r.setStroke(outlineColor);
        r.setStrokeWidth(thickness);
        sList.add(r);

        r.setOnMouseClicked(e -> {
            try{
                if(workarea.getCursor().equals(Cursor.DEFAULT)){
                    r.setStyle("-fx-stroke: yellow;" +
                            "-fx-stroke-width:5");
                    for(Shape check : sList){
                        if(check.getStyle().contains("yellow") && (!check.equals(r))){
                            check.setStroke(workspace.getOutlineColorPicker());
                            check.setStrokeWidth(workspace.getOutlineThicknessSlider());
                            check.setStyle("");
                        }
                    }
                    workspace.setFillColorPicker((Color)r.getFill());
                    if(!(r.getStroke().toString().contains("0xffff00"))){
                        workspace.setOutlineColorPicker((Color)r.getStroke());
                        workspace.setOutlineThicknessSlider(r.getStrokeWidth()*10);  
                    }
                }
            }
            catch(Exception ex){
                
            }
            workspace.refreshButtons();
        });
        r.setOnMouseDragged(e -> {
            if(workarea.getCursor().equals(Cursor.DEFAULT)){
                r.setX(e.getX());
                r.setY(e.getY());
            }
        });
        
        workarea.getChildren().add(r);
        return r;
        
    }
    public Ellipse handleAddEllipse(Point2D location, Pane workarea, Color fillColor, Color outlineColor, double thickness, double x, double y) {
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        Ellipse el = new Ellipse(location.getX(), location.getY(), x, y);
        el.setFill(fillColor);
        el.setStroke(outlineColor);
        el.setStrokeWidth(thickness);
        sList.add(el);
        
        el.setOnMouseClicked(e -> {
            try{
                if(workarea.getCursor().equals(Cursor.DEFAULT)){
                    el.setStyle("-fx-stroke: yellow;" +
                            "-fx-stroke-width:5");
                    for(Shape check : sList){
                        if(check.getStyle().contains("yellow") && (!check.equals(el))){
                            check.setStroke(workspace.getOutlineColorPicker());
                            check.setStrokeWidth(workspace.getOutlineThicknessSlider());
                            check.setStyle("");
                        }
                    }

                    workspace.setFillColorPicker((Color)el.getFill());
                    if(!(el.getStroke().toString().contains("0xffff00"))){
                        workspace.setOutlineColorPicker((Color)el.getStroke());
                        workspace.setOutlineThicknessSlider(el.getStrokeWidth()*10);  
                    }
                }
                workspace.refreshButtons();
            }
            catch(Exception ex){
                        
            }
        });
        el.setOnMouseDragged(e ->{
            if(workarea.getCursor().equals(Cursor.DEFAULT)){
                el.setCenterX(e.getX());
                el.setCenterY(e.getY());
            }
        });
        
        workarea.getChildren().add(el);
        return el;
    }
    public void removeShape(Shape remove){
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        workspace.getWorkspace().getChildren().remove(remove);
        sList.remove(remove);
    }
    public void changeFillColor(Shape change, Color fillColor){
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        change.setFill(fillColor);
    }
    public void changeOutlineColor(Shape change, Color outlineColor){
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        change.setStroke(outlineColor);
    }
    public void changeOutlineThickness(Shape change, double thickness){
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        change.setStrokeWidth(thickness);
    }
    public Shape isHighlighted(){
        for(Shape sTest : sList){
            if(sTest.getStyle().contains("yellow")){
                return sTest;
            }
        }
        return null;
    }
    public void addList(Shape add){
        sList.add(add);
    }
    public void clearList(){

        sList.clear();
    }
    public ArrayList<Shape> getList(){
        System.out.println(sList.size());
        return sList;
    }
}   
