package pm.data;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Collection;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Shape;
import pm.PoseMaker;
import pm.gui.Workspace;
import saf.components.AppDataComponent;
import saf.AppTemplate;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Ellipse;
import pm.controller.PageEditController;

/**
 * This class serves as the data management component for this application.
 *
 * @author Richard McKenna
 * @author ?
 * @version 1.0
 */
public class DataManager implements AppDataComponent {
    // THIS IS A SHARED REFERENCE TO THE APPLICATION
    AppTemplate app;

    /**
     * THis constructor creates the data manager and sets up the
     *
     *
     * @param initApp The application within which this data manager is serving.
     */
    public DataManager(AppTemplate initApp) throws Exception {
	// KEEP THE APP FOR LATER
	app = initApp;
    }
    public ArrayList<Rectangle> getRectangle(){
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        return workspace.getRectangles();
    }
    public ArrayList<Ellipse> getEllipse(){
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        return workspace.getEllipses();
    }
    public void makeRectangle(double height, double width, String fillColor, String outlineColor, double thickness, double x, double y){
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        Pane workarea = workspace.getWorkspace();
        PageEditController pageEditController = new PageEditController((PoseMaker) app);
        Paint fill = Paint.valueOf(fillColor);
        Paint outline = Paint.valueOf(outlineColor);
        
        Color fillX = (Color)fill;
        Color fillOut = (Color) outline;
        workspace.addRectangles(pageEditController.handleAddSquare(new Point2D(x,y), workarea, fillX, fillOut, thickness, width, height));
        
    }
    public void makeEllipse(double height, double width, String fillColor, String outlineColor, double thickness, double x, double y){
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        Pane workarea = workspace.getWorkspace();
        PageEditController pageEditController = new PageEditController((PoseMaker)app);
        Paint fill = Paint.valueOf(fillColor);
        Paint outline = Paint.valueOf(outlineColor);
        
        Color fillX = (Color)fill;
        Color fillOut = (Color) outline;
        workspace.addEllipses(pageEditController.handleAddEllipse(new Point2D(x,y), workarea, fillX, fillOut, thickness, height, width));
    }
    public String getBackgroundColor(){
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        return workspace.getWorkspace().getStyle();
    }
    public void setBackgroundColor(String bgColor){
        bgColor = bgColor.substring(1, bgColor.length()-1);
        app.getWorkspaceComponent().getWorkspace().setStyle(bgColor);
    }
    /**
     * This function clears out the HTML tree and reloads it with the minimal
     * tags, like html, head, and body such that the user can begin editing a
     * page.
     */
    @Override
    public void reset() {
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        PageEditController pageEditController = new PageEditController((PoseMaker) app);
        pageEditController.clearList();
        workspace.clearList();
        workspace.getWorkspace().setStyle("");
        workspace.getWorkspace().getChildren().clear();
        workspace.getWorkspace().getChildren().add(workspace.getToolbar());
        
    }
}
