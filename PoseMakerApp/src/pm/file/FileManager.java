package pm.file;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.Rectangle;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonString;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;
import saf.components.AppDataComponent;
import saf.components.AppFileComponent;
import pm.controller.PageEditController;
import pm.PoseMaker;
import pm.data.DataManager;
import pm.gui.Workspace;
import saf.AppTemplate;

/**
 * This class serves as the file management component for this application,
 * providing all I/O services.
 *
 * @author Richard McKenna
 * @author ?
 * @version 1.0
 */
public class FileManager implements AppFileComponent {
    AppTemplate app;
    /**
     * This method is for saving user work, which in the case of this
     * application means the data that constitutes the page DOM.
     * 
     * @param data The data management component for this application.
     * 
     * @param filePath Path (including file name/extension) to where
     * to save the data to.
     * 
     * @throws IOException Thrown should there be an error writing 
     * out data to the file.
     */
    @Override
    public void saveData(AppDataComponent data, String filePath) throws IOException {     
        StringWriter sw = new StringWriter();
        DataManager dataManager = (DataManager)data;
        ArrayList<Rectangle> rList = dataManager.getRectangle();
        ArrayList<Ellipse> eList = dataManager.getEllipse();
        
        JsonArrayBuilder rectangleBuilder = Json.createArrayBuilder();
        fillArray(rList, rectangleBuilder);
        JsonArray nodesArray = rectangleBuilder.build();
        
        JsonArrayBuilder ellipseBuilder = Json.createArrayBuilder();
        fillEllipse(eList, ellipseBuilder);
        JsonArray ellipseArray = ellipseBuilder.build();
        
        JsonObject dataManagerJSO = Json.createObjectBuilder()
                .add("background", dataManager.getBackgroundColor())
                .add("rectangle", nodesArray)
                .add("ellipse", ellipseArray)
                .build();

	Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);
	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.writeObject(dataManagerJSO);
	jsonWriter.close();

	// INIT THE WRITER
	OutputStream os = new FileOutputStream(filePath);
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(dataManagerJSO);
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter(filePath);
	pw.write(prettyPrinted);
	pw.close();
    }
    private void fillArray(ArrayList<Rectangle> rList, JsonArrayBuilder arrayBuilder){
        for(Rectangle r : rList){
            JsonObject shape = makeShapeJsonObject(r);
            arrayBuilder.add(shape);
        }
    }
    private void fillEllipse(ArrayList<Ellipse> eList, JsonArrayBuilder arrayBuilder){
        for (Ellipse e : eList){
            JsonObject shape = makeShapeJsonObject(e);
            arrayBuilder.add(shape);
        }
    }
    private JsonObject makeShapeJsonObject(Rectangle r){        
        JsonObject jso = Json.createObjectBuilder()
                .add("height", r.getHeight())
                .add("width",r.getWidth())
                .add("x", r.getX())
                .add("y",r.getY())
                .add("fill", r.getFill().toString())
                .add("stroke",r.getStroke().toString())
                .add("thickness", r.getStrokeWidth())
                .build();
        return jso;        
    }
    private JsonObject makeShapeJsonObject(Ellipse e){
        JsonObject jso = Json.createObjectBuilder()
                .add("height", e.getRadiusX())
                .add("width", e.getRadiusY())
                .add("x", e.getCenterX())
                .add("y", e.getCenterY())
                .add("fill", e.getFill().toString())
                .add("stroke", e.getStroke().toString())
                .add("thickness", e.getStrokeWidth())
                .build();
        return jso;   
    }
    /**
     * This method loads data from a JSON formatted file into the data 
     * management component and then forces the updating of the workspace
     * such that the user may edit the data.
     * 
     * @param data Data management component where we'll load the file into.
     * 
     * @param filePath Path (including file name/extension) to where
     * to load the data from.
     * 
     * @throws IOException Thrown should there be an error reading
     * in data from the file.
     */
    
    @Override
    public void loadData(AppDataComponent data, String filePath) throws IOException {
        DataManager dataManager = (DataManager) data;
        dataManager.reset();

        JsonObject json = loadJSONFile(filePath);
        
        JsonArray jsonRectangle = json.getJsonArray("rectangle");
        loadRectangle(jsonRectangle, dataManager);
        JsonArray jsonEllipse = json.getJsonArray("ellipse");
        loadEllipse(jsonEllipse, dataManager);
        
        JsonString jsonBackground = json.getJsonString("background");
        dataManager.setBackgroundColor(jsonBackground.toString());
    }
    private void loadRectangle(JsonArray jsonArray, DataManager dataManager){
        for(int i = 0; i < jsonArray.size(); i++){
            JsonObject shapeJso = jsonArray.getJsonObject(i);
            double height = Double.parseDouble(shapeJso.getJsonNumber("height").toString());
            double width = Double.parseDouble(shapeJso.getJsonNumber("width").toString());
            double x = Double.parseDouble(shapeJso.getJsonNumber("x").toString());
            double y = Double.parseDouble(shapeJso.getJsonNumber("y").toString());
            String fill = shapeJso.getString("fill");
            String stroke = shapeJso.getString("stroke");
            double thickness = Double.parseDouble(shapeJso.getJsonNumber("thickness").toString());
            
            dataManager.makeRectangle(height, width, fill, stroke, thickness, x, y);
        }
    }
    private void loadEllipse(JsonArray jsonArray, DataManager dataManager){
        for(int i = 0; i < jsonArray.size(); i++){
            JsonObject shapeJso = jsonArray.getJsonObject(i);
            double height = Double.parseDouble(shapeJso.getJsonNumber("height").toString());
            double width = Double.parseDouble(shapeJso.getJsonNumber("width").toString());
            double x = Double.parseDouble(shapeJso.getJsonNumber("x").toString());
            double y = Double.parseDouble(shapeJso.getJsonNumber("y").toString());
            String fill = shapeJso.getString("fill");
            String stroke = shapeJso.getString("stroke");
            double thickness = Double.parseDouble(shapeJso.getJsonNumber("thickness").toString());
            
            dataManager.makeEllipse(height, width, fill, stroke, thickness, x, y);
        }
    }
    // HELPER METHOD FOR LOADING DATA FROM A JSON FORMAT
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
	InputStream is = new FileInputStream(jsonFilePath);
	JsonReader jsonReader = Json.createReader(is);
	JsonObject json = jsonReader.readObject();
	jsonReader.close();
	is.close();
	return json;
    }
    
    /**
     * This method exports the contents of the data manager to a 
     * Web page including the html page, needed directories, and
     * the CSS file.
     * 
     * @param data The data management component.
     * 
     * @param filePath Path (including file name/extension) to where
     * to export the page to.
     * 
     * @throws IOException Thrown should there be an error writing
     * out data to the file.
     */
    @Override
    public void exportData(AppDataComponent data, String filePath) throws IOException {

    }
    
    /**
     * This method is provided to satisfy the compiler, but it
     * is not used by this application.
     */
    @Override
    public void importData(AppDataComponent data, String filePath) throws IOException {
	// NOTE THAT THE Web Page Maker APPLICATION MAKES
	// NO USE OF THIS METHOD SINCE IT NEVER IMPORTS
	// EXPORTED WEB PAGES
    }
}
